<?php
namespace App\Utils\Inyo\Api\Commands;

class Transaction extends AbstractCommand
{

    private $transaction_id="";

    public function setTransactionId($transaction_id)
    {
        $this->transaction_id = $transaction_id;
        return $this;
    }

    public function list()
    {
        return $this->transaction_id == "" ? 
            [] :
            $this->sendCachedCommand("get", env('GMT_ENDPOINT')."/organizations/".env('GMT_ORG')."/fx/transaction/external/".$this->transaction_id, []);
    }

    public function update($args)
    {
        throw new \Exception("not implemented yet");
    }

    public function save($args)
    {
        $response = $this->sendRawCommand("post", env('GMT_ENDPOINT')."/organizations/".env('GMT_ORG')."/fx/transaction/external", $args);
        return $response;

    }

}