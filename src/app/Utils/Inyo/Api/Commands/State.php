<?php
namespace App\Utils\Inyo\Api\Commands;

class State extends AbstractCommand
{
    protected $country_id="";

    public function setCountryId($country_id)
    {
        $this->country_id = $country_id;
        return $this;
    }

    public function list()
    {
        return $this->country_id == "" ? 
                    [] :
                    $this->sendCachedCommand("get", env('GMT_ENDPOINT')."/countries/".$this->country_id."/states", ["countryName" => ""]);
    }

}