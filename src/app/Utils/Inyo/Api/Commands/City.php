<?php
namespace App\Utils\Inyo\Api\Commands;

class City extends AbstractCommand
{

    private $state_id="";

    public function setStateId($state_id)
    {
        $this->state_id = $state_id;
        return $this;
    }

    public function list()
    {
        return $this->state_id == "" ? 
            [] :
            $this->sendCachedCommand("get", env('GMT_ENDPOINT')."/states/".$this->state_id."/cities", ["countryName" => ""]);
    }

}