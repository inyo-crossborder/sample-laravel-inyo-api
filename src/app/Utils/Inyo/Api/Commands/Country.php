<?php
namespace App\Utils\Inyo\Api\Commands;

class Country extends AbstractCommand
{

    public function list()
    {
        return $this->sendCachedCommand("get", env("GMT_ENDPOINT")."/countries", ["countryName" => ""]);
    }

}