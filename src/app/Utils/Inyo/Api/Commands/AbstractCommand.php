<?php

namespace App\Utils\Inyo\Api\Commands;


use Cache;
use Illuminate\Support\Facades\Http;
use Log;

abstract class AbstractCommand
{


    protected function sendRawCommand($method, $url, $params)
    {
        return Http::asJson()
                    ->withHeaders([
                        'x-api-key' => env('GMT_API_KEY')
                    ])          
                    ->$method( $url, $params );
                    
    }

    protected function sendUploadCommand($url, $path, $params)
    {
        return Http::attach('file', file_get_contents($path), 'file.jpg')
                ->withHeaders([
                    'x-api-key' => env('GMT_API_KEY')
                ])
                ->post($url, $params);
    }

    protected function sendCachedCommand($method, $url, $params)
    {
        $key = sha1( $method."_".$url."_".json_encode($params) );
        Log::debug('Cached key '. $method."_".$url."_".json_encode($params) );

        if(Cache::has($key))
        {
            Log::debug('From cache '. $key);
            return Cache::get($key);
        }
        else
        {
            $response = $this->sendRawCommand($method, $url, $params);

            if($response->successful())
            {
                Cache::put($key, $response->json(), 3600); //1 hr
                return $response->json();
            }
            else
            {
                return [];
            }
        }

    }

    abstract public function list();

    public static function getInstance(AbstractCommand $class)
    {
        return app($class);
    }

    protected function getToken()
    {
	    if(Cache::has('s_gmt_token'))
	    {
		    return Cache::get('_gmt_token');
	    }
	    else
	    {


		    $response = Http::asJson()
                        ->post(env('GMT_ENDPOINT')."/users/login",[
                            "username" => $this->username,
                            "password" => $this->password
        	]);

        	if($response->successful())
        	{
                	$json = $response->json();
                	Cache::put('_gmt_token', $json, $json['expiresIn']-5);

                	return $json;
        	}
            
            
	}


}


}