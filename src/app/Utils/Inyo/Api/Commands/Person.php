<?php
namespace App\Utils\Inyo\Api\Commands;

class Person extends AbstractCommand
{

    private $person_id="";

    public function setPersonId($person_id)
    {
        $this->person_id = $person_id;
        return $this;
    }

    public function list()
    {
        return $this->person_id == "" ? 
            [] :
            $this->sendCachedCommand("get", env('GMT_ENDPOINT')."/organizations/".env('GMT_ORG')."/people/".$this->person_id, []);
    }

    public function uploadDoc($filename, $args)
    {
        if(file_exists($filename))
        {
            return $this->sendUploadCommand(
                            env('GMT_ENDPOINT')."/organizations/".env('GMT_ORG')."/people/".$this->person_id."/nationalId/upload", 
                            $filename,
                            $args);
        }
        else
            throw new \Exception("file doesnt exist");
    }

    public function update($args)
    {
        throw new \Exception("Not implemented yet");
    }

    public function save($args)
    {
        $response = $this->sendRawCommand("post", env('GMT_ENDPOINT')."/organizations/".env('GMT_ORG')."/people", $args);
        return $response;

    }

}