<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cache;
use App\Utils\Inyo\Api\Commands\Transaction;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    

        $v = $request->validate([
            "senderId" => "required",
            "receiverId" => "required",
            "originalCurrency" => "required",
            "destinationCurrency" => "required",
            "exchangeRate" => "required",
            "totalAmount" => "required",
            "fee" => "required",
            "receivingAmount" => "required",
            "conversionAmount" => "required",
            "billingAddress.cityId" => "required|integer",
            "billingAddress.line1" => "required",
            "billingAddress.line2" => "required",
            "billingAddress.zipcode" => "required",
            "payment.provider" => "required",
            "payment.type" => "required",
            "payment.authorizationId" => "required",
            "payment.externalId" => "required"

        ]); 
    
        $transaction = app(Transaction::class);
        $r = $transaction->save($v);
    
        // temporarily add the saved id to cache. 
        // real time app should persist this
        if($r->ok())
        {
            $persons = [];
    
            if(Cache::has('persons'))
                $persons=Cache::get('persons');
            
            $json = $r->json();
            $persons[$json['id']] = $json;
    
            Cache::put('persons', $persons, 3600*24*365); //1 year
        }
        else
        {
            dd($r);
        }
    
        return redirect('/');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $person = Cache::get('persons')[$id];

        return view('transaction', ['person' => $person, 'transactions' => [] ] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
