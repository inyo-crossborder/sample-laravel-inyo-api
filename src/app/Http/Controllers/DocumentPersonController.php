<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cache;
use App\Utils\Inyo\Api\Commands\Person;

use function PHPSTORM_META\elementType;

class DocumentPersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validation
        $v = $request->validate([
            'file' => 'required|mimes:jpeg,jpg,png|max:2048',
            'issuer' => 'required',
            'idNumber' => 'required',
            'expirationDate' => 'required|date_format:Y-m-d',
            'personId' => 'required'
        ]);

        if($request->file('file')) 
        {

            $filename = time().'_'.$request->file->getClientOriginalName();
            $dir = storage_path('');
            $request->file->move($dir, $filename);

            $p = app(Person::class)
                    ->setPersonId( $v['personId'] )
                    ->uploadDoc( $dir."/". $filename, [
                        'issuer'=>'FL', 
                        'idNumber' => '12345',
                        'expirationDate' => '2029-01-01'
                        ]);

            unlink($dir."/". $filename);

            if($p->ok())
            {
                
                $json = $p->json();
                $documents = [];
    
                if(Cache::has('documents'))
                    $documents=Cache::get('documents');
            
                $json['personId'] = $v['personId'];
                $documents[] = $json;
    
                Cache::put('documents', $documents, 3600*24*365); //1 year
            }
            else
            {
                return redirect()->back()->withErrors(['errors' => 'Error executing the call']);
            }

            return redirect()->back();

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $p = Cache::get('persons')[$id];
        $d = collect(Cache::get('documents'))->where('personId', $id);

        return view('document', ['person' => $p, 'documents' => $d]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
