<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Utils\Inyo\Api\Commands\Country;
use App\Utils\Inyo\Api\Commands\City;
use App\Utils\Inyo\Api\Commands\State;
use Cache;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $country_id=""; 
        $state_id="";

        if($request->has('country_id'))
            $country_id = $request->get('country_id');

        if($request->has('state_id'))
            $state_id = $request->get('state_id');

        $c = app(Country::class);
        $s = app(State::class)->setCountryId($country_id);
        $ct = app(City::class)->setStateId($state_id);


        return view('home', 
				['countries' => $c->list(), 
				'states' => $s->list(), 
				'cities' => $ct->list(),
				'persons' => Cache::get('persons')
				]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
