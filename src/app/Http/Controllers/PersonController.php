<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Utils\Inyo\Api\Commands\Person;
use Cache;

class PersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = $request->validate([
            "firstName" => "required",
            "lastName" => "required",
            "mobilePhone" => "required",
            "email" => "required|email",
            "birthDate" => "required|date_format:Y-m-d",
            "gender" => "required",
            "address.cityId" => "required",
            "address.line1" => "required",
            "address.line2" => "sometimes",
            "address.zipcode" => "required|regex:/[0-9]+/"
        ]);
    
        $p = app(Person::class);

        $r = $p->save($v);
    
        // temporarily add the saved id to cache. 
        // real time app should persist this
        if($r->ok())
        {
            $persons = [];
    
            if(Cache::has('persons'))
                $persons=Cache::get('persons');
            
            $json = $r->json();
            $persons[$json['id']] = $json;
    
            Cache::put('persons', $persons, 3600*24*365); //1 year
        }
    
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
