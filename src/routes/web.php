<?php

use Illuminate\Support\Facades\Route;
use App\Utils\Inyo\Api\Commands\Person;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;

Route::get('/', '\App\Http\Controllers\HomeController@index');
Route::post('/addperson', '\App\Http\Controllers\PersonController@store');
Route::resource('/transaction', '\App\Http\Controllers\TransactionController');
Route::resource('/document', '\App\Http\Controllers\DocumentPersonController');

Route::get('/uploadtest', function(){

    $x= app(Person::class);
    $response = $x->setPersonId('01c5b9e3-b016-4033-b75e-22a6b85428c4')
                    ->uploadDoc( storage_path('app/bope.jpeg'), [
                                                    'issuer'=>'FL', 
                                                    'idNumber' => '12345',
                                                    'expirationDate' => '2029-01-01'
                                                    ]);
    if($response->ok())
    {
        echo "upload sent";
    }
    else
    {
        echo "upload error uploading";
    }

});