<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="https://getbootstrap.com/docs/4.0/assets/img/favicons/favicon.ico">

  <title>Person</title>

  <!-- Bootstrap core CSS -->
  <link href="https://getbootstrap.com/docs/4.0/dist/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
</head>

<body class="m-3">
  <form action="/addperson" method="POST">
  <div class="row">
    <div class="col-4">&nbsp;</div>
    <div class="col-4">
      <div class="row">
        <div class="col-4">Country: </div>
        <div class="col-4">
          <select name="country" id="country">
            @foreach($countries as $c)
            <option value="{{ $c['code'] }}" {{ Request::get('country_id') == $c['code'] ? 'selected' : ''}}>{{$c['name']}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="row">
        <div class="col-4">State: </div>
        <div class="col-4">
          <select name="state" id="state">
            @foreach($states as $s)
            <option value="{{ $s['id'] }}" {{ Request::get('state_id') == $s['id'] ? 'selected' : ''}}>{{$s['name']}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="row">
        <div class="col-4">City: </div>
        <div class="col-4">
          <select name="address[cityId]" id="cityId">
            @foreach($cities as $ct)
            <option value="{{ $ct['id'] }}" {{ Request::get('city_id') == $ct['id'] ? 'selected' : ''}}>{{$ct['name']}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="row">
        <div class="col-4">First name: </div>
        <div class="col-4"><input type="text" name="firstName" value=""></div>
      </div>

      <div class="row">
        <div class="col-4">Last name: </div>
        <div class="col-4"><input type="text" name="lastName" value=""></div>
      </div>

      <div class="row">
        <div class="col-4">Mobile phone: </div>
        <div class="col-4"><input type="text" name="mobilePhone" value=""></div>
      </div>

      <div class="row">
        <div class="col-4">Birth Date: </div>
        <div class="col-4"><input type="text" name="birthDate" value=""></div>
      </div>

      <div class="row">
        <div class="col-4">Email: </div>
        <div class="col-4"><input type="email" name="email" value=""></div>
      </div>

      <div class="row">
        <div class="col-4">Gender: </div>
        <div class="col-4">
            <select name= "gender">
              <option value="Male">Male</option>
              <option value="Female">Female</option>

            </select>
        </div>
      </div>

      <div class="row">
        <div class="col-12"><hr />Address: <hr /> </div>
      </div>

      <div class="row">
        <div class="col-4">Address: </div>
        <div class="col-4"><input type="text" name="address[line1]" value=""></div>
      </div>

      <div class="row">
        <div class="col-4">Complement: </div>
        <div class="col-4"><input type="text" name="address[line2]" value=""></div>
      </div>

      <div class="row">
        <div class="col-4">Zipcode: </div>
        <div class="col-4"><input type="text" name="address[zipcode]" value=""></div>
      </div>
     
       @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


      <div class="row">
        <div class="col-4">
          <button class="btn btn-primary btn-block" type="submit">Add a person</button>
        </div>
      </div>

      @csrf
    </form>
</div>
<div class="col-4">&nbsp;</div>
</div>
  <hr />

  @foreach($persons as $k=>$v)
  <div class="row">

    <div class="col-4">{{ $v['id'] }}</div>
    <div class="col-2">{{ $v['firstName'] }}</div>
    <div class="col-2">{{ $v['lastName'] }}</div>
    <div class="col-2">{{ $v['email'] }}</div>
    <div class="col-1">[<a href="/transaction/{{ $v['id'] }}">+ txn</a>]</div>
    <div class="col-1">[<a href="/document/{{ $v['id'] }}">+ dcto</a>]</div>
    

  </div>
  @endforeach

</body>
<script src="https://code.jquery.com/jquery-3.7.0.slim.min.js" integrity="sha256-tG5mcZUtJsZvyKAxYLVXrmjKBVLd6VpVccqz/r4ypFE=" crossorigin="anonymous"></script>

<script>
  $(window).ready(function() {

    $('#country').change(function() {

      window.location = '/?country_id=' + $(this).val();
    });

    $('#state').change(function() {

      window.location = '/?country_id=' + $('#country').val() + '&state_id=' + $(this).val();
    });

  });
</script>


</html>