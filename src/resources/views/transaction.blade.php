<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="https://getbootstrap.com/docs/4.0/assets/img/favicons/favicon.ico">

  <title>Transaction</title>

  <!-- Bootstrap core CSS -->
  <link href="https://getbootstrap.com/docs/4.0/dist/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
</head>

<body class="m-3">
  <form action="/transaction" method="POST">
  <div class="row">
    <div class="col-2">&nbsp;</div>
    <div class="col-6">

      <input type="hidden" name="senderId" value="{{ $person['id'] }}" />
      <input type="hidden" name="receiverId" value="{{ $person['id'] }}" />
      <input type="hidden" name="originalCurrency" value="USD" />
      <input type="hidden" name="billingAddress[cityId]" value="121746" />

      <div class="row">
        <div class="col-4">First name: </div>
        <div class="col-8">{{ $person['firstName'] }}</div>
      </div>

      <div class="row">
        <div class="col-4">Last name: </div>
        <div class="col-8">{{ $person['lastName'] }}</div>
      </div>

      <div class="row">
        <div class="col-4">Mobile phone: </div>
        <div class="col-8">{{ $person['mobilePhone'] }}</div>
      </div>

      <div class="row">
        <div class="col-12"><hr />Currency: <hr /> </div>
      </div>

      <div class="row">
        <div class="col-4">Original Currency: </div>
        <div class="col-8">USD</div>
      </div>

      <div class="row">
        <div class="col-4">Destination Currency: </div>
        <div class="col-8">
          <select name="destinationCurrency">
            <option value="EUR">Euros</option>
            <option value="CAD">Canadian dollars</option>
            <option value="BRL">Brazilian real</option>
          </select>
        </div>
      </div>

      <div class="row">
        <div class="col-4">Total amount: </div>
        <div class="col-8"><input type="text" name="totalAmount" value="1030"></div>
      </div>

      <div class="row">
        <div class="col-4">Fee: </div>
        <div class="col-8"><input type="text" name="fee" value="30"></div>
      </div>

      <div class="row">
        <div class="col-4">Conversion amount: </div>
        <div class="col-8"><input type="text" name="conversionAmount" value="100"></div>
      </div>

      <div class="row">
        <div class="col-4">FX rate: </div>
        <div class="col-8"><input type="text" name="exchangeRate" value="0.93"></div>
      </div>

      <div class="row">
        <div class="col-4">Receiving amount: </div>
        <div class="col-8"><input type="text" name="receivingAmount" value="930"></div>
      </div>

      <div class="row">
        <div class="col-12"><hr />Payment: <hr /> </div>
      </div>

      <div class="row">
        <div class="col-4">Provider: </div>
        <div class="col-8">
          <select name="payment[provider]">
            <option value="checkout.com">Checkout.com</option>
            <option value="vantiv">Vantiv</option>
          </select>
        </div>
      </div>

      <div class="row">
        <div class="col-4">Type: </div>
        <div class="col-8">
          <select name="payment[type]">
            <option value="debit">Debit</option>
            <option value="credit">Credit card</option>
            <option value="ach">ACH</option>
            <option value="cash">Cash</option>
          </select>
        </div>
      </div>

      <div class="row">
        <div class="col-4">Authorization id: (from checkout.com) </div>
        <div class="col-8"><input type="text" name="payment[authorizationId]" value="5678"></div>
      </div>

      <div class="row">
        <div class="col-4">External id (from spendology): </div>
        <div class="col-8"><input type="text" name="payment[externalId]" value="1234"></div>
      </div>

      <div class="row">
        <div class="col-12"><hr />Billing Address: <hr /> </div>
      </div>

      <div class="row">
        <div class="col-4">Address: </div>
        <div class="col-8"><input type="text" name="billingAddress[line1]" value="1101 brickell ave"></div>
      </div>

      <div class="row">
        <div class="col-4">Complement: </div>
        <div class="col-8"><input type="text" name="billingAddress[line2]" value="suite 201"></div>
      </div>

      <div class="row">
        <div class="col-4">Zipcode: </div>
        <div class="col-4"><input type="text" name="billingAddress[zipcode]" value="33131"></div>
      </div>
     
       @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


      <div class="row">
        <div class="col-4">
          <button class="btn btn-primary btn-block" type="submit">Save transaction</button>
        </div>
      </div>

      @csrf
    </form>
</div>
<div class="col-4">&nbsp;</div>
</div>
  <hr />

  @foreach($transactions as $k=>$v)
  <div class="row">

    <div class="col-3">{{ $v['id'] }}</div>
    <div class="col-3">{{ $v['firstName'] }}</div>
    <div class="col-3">{{ $v['lastName'] }}</div>
    <div class="col-3">{{ $v['email'] }} [<a href="/new-transaction/{{ $v['id'] }}">+ transaction</a>]</div>

  </div>
  @endforeach

</body>
<script src="https://code.jquery.com/jquery-3.7.0.slim.min.js" integrity="sha256-tG5mcZUtJsZvyKAxYLVXrmjKBVLd6VpVccqz/r4ypFE=" crossorigin="anonymous"></script>

<script>
  $(window).ready(function() {


  });
</script>


</html>