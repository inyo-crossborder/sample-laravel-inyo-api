<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="https://getbootstrap.com/docs/4.0/assets/img/favicons/favicon.ico">

  <title>Document</title>

  <!-- Bootstrap core CSS -->
  <link href="https://getbootstrap.com/docs/4.0/dist/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
</head>

<body class="m-3">
<form action="/document" enctype="multipart/form-data" method="POST">
  @csrf
  <div class="row">
    <div class="col-2">&nbsp;</div>
    <div class="col-6">

      <input type="hidden" name="personId" value="{{ $person['id'] }}" />

      <div class="row">
        <div class="col-4">First name: </div>
        <div class="col-8">{{ $person['firstName'] }}</div>
      </div>

      <div class="row">
        <div class="col-4">Last name: </div>
        <div class="col-8">{{ $person['lastName'] }}</div>
      </div>

      <div class="row">
        <div class="col-12"><hr />Document: <hr /> </div>
      </div>

      <div class="row">
        <div class="col-4">Type: </div>
        <div class="col-8">
          <select name="documentType">
            <option value="NID">National ID</option>
            <option value="PASSPORT">Passport</option>
          </select>
        </div>
      </div>

      <div class="row">
        <div class="col-4">File: </div>
        <div class="col-8">
            <div class="input-group mt-3">
              <div class="custom-file">
                  <input type="file" name="file" class="form-control" id="customFile" />
              </div>
            </div>
        </div>
      </div>

      <div class="row">
        <div class="col-4">Issuer: </div>
        <div class="col-8"><input type="text" name="issuer" value="FL"></div>
      </div>

      <div class="row">
        <div class="col-4">ID Number: </div>
        <div class="col-8"><input type="text" name="idNumber" value="12345"></div>
      </div>

      <div class="row">
        <div class="col-4">Expiration Date: </div>
        <div class="col-8"><input type="text" name="expirationDate" value="2028-10-10"></div>
      </div>

     
       @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


      <div class="row">
        <div class="col-4">
          <button class="btn btn-primary btn-block" type="submit">Upload</button>
        </div>
      </div>

      @csrf
    </form>
</div>
<div class="col-4">&nbsp;</div>
</div>
  <hr />

  @foreach($documents as $d)
  <div class="row">

    <div class="col-3">{{ $d['id'] }}</div>
    <div class="col-3">{{ $d['issuer'] }}</div>
    <div class="col-3">{{ $d['type'] }}</div>
    <div class="col-3">{{ $d['fileName'] }}</div>

  </div>
  @endforeach

</body>
<script src="https://code.jquery.com/jquery-3.7.0.slim.min.js" integrity="sha256-tG5mcZUtJsZvyKAxYLVXrmjKBVLd6VpVccqz/r4ypFE=" crossorigin="anonymous"></script>

<script>
  $(window).ready(function() {


  });
</script>


</html>