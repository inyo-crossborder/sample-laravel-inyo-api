## Sample laravel project for the API 

To run this project you must have docker and docker-compose installed.
                                                                         
1) Clone the repo
                    
2) Create the required network adapter

docker network create web-net

3) Install the dependencies (this will take a while)

./docker/scripts/composer.sh install
                    
4) Copy the env key and edit the API key params 

cp src/.env.example src/.env

5) Generate the app keys 

./docker/scripts/artisan.sh keys:generate

6) Launch docker

cd docker             
docker-compose up     
                      
7) Server will listen at port 8083. Sample project supports synchronizing country, state, city, as well as creating person and transactions. More updates to come.

## Utililty classes
API classes can be found at src/app/Utils

Sample api call can be made via console:
./docker/scripts/artisan.sh tinker

use App\Utils\Inyo\Api\Commands\Country;
$countries = app(Country::class)->list();
